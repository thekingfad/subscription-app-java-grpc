package com.ardab.subsapp.subscriber.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Subscriber {

    public Subscriber(){

    }

    public Subscriber(String subscriberMsisdn, String subscriberName, String subscriberSurname, double subscriberBalance) {
        this.subscriberMsisdn = subscriberMsisdn;
        this.subscriberName = subscriberName;
        this.subscriberSurname = subscriberSurname;
        this.subscriberBalance = subscriberBalance;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String subscriberMsisdn;

    private String subscriberName;
    private String subscriberSurname;
    private double subscriberBalance;
    private int subscriptionPackage;
}
