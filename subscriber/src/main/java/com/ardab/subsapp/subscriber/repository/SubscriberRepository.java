package com.ardab.subsapp.subscriber.repository;

import com.ardab.subsapp.subscriber.model.Subscriber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Optional;

@EnableJpaRepositories
public interface SubscriberRepository extends JpaRepository<Subscriber, Long> {
}
