package com.ardab.subsapp.mobilepackage.repository;

import com.ardab.subsapp.mobilepackage.model.MobilePackage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MobilePackageRepository extends JpaRepository<MobilePackage, Integer> {
}
